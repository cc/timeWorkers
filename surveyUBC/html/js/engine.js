// engine.js 
// parent thread interfaces with audio context (not using audioworklet, yet because not in FF)
// spawns child WorkerThreads and manages their lifecycle 

// check whether chrome or firefox -- no others 
var testedBrowser = null
if(navigator.userAgent.indexOf("Chrome") != -1 )
    {
        testedBrowser = "chrome"
    }
if(navigator.userAgent.indexOf("Firefox") != -1 ) 
    {
        testedBrowser = "firefox"
    }
if (testedBrowser === null) alert("successfully tested on firefox, chrome, some edge -- may fail on safari and others, continue and find out")

// specify local data file using drag and drop
// when running local, user intervention is required by chrome
// modified from http://html5demos.com/file-api
var holder = document.getElementById('holder')
function setupDragAndDrop() {
  if(holder!=null) {
    holder.innerHTML = '<br><br><br>drag a data file here to play it'
    holder.ondragover = function() {
      this.className = 'hover'
      return false
    }
    holder.ondragend = function() {
      this.className = ''
      return false
    }
    holder.ondrop = function(e) {
      this.className = ''
      e.preventDefault()
      let file = e.dataTransfer.files[0]
      reader = new FileReader()
      reader.onload = function(event) {
        draggedData(event.target.result)
      }
      reader.readAsText(file)
      holder.innerHTML = "<br><br><br>"+file.name
      return false
    }
  }
}
if(holder!=null) setupDragAndDrop()

var cachedColumnOfData = null
// play data contained in dragged in file 
function draggedData(columnOfData) {
  cachedColumnOfData = columnOfData
  context = new (window.AudioContext || window.webkitAudioContext)()
  for (let i = 0; i < nWorkers; i++)  makeWorkerThread() // 1 per wwckScript for now
  cushion.oninput()
//  slider.oninput() 
  for (let i = 0; i < workerThreads.length; i++)   
    workerThreads[i].go(columnOfData)
// UI while playing
  buttonEnable("stop", "icons/120px-Gtk-media-stop.svg.png")
  buttonDisable("dataFileButton", "icons/120px-Gtk-media-play-ltr.alt.png")
  buttonDisable("demo", "icons/48px-Gnome-web-browser.svg.png")
  hide("demoLabel")
  hide("demo")
}

///////////////////////
var nWorkers = 1 // could have parallel workers, but not in this example
var context
var workerThreads = []
var uwta = [] // ugenWorkerThreadsArrays, set of all ugens in a wt

// get and set document elements
function buttonEnable(name, img) {
  let button = document.getElementById(name)
  button.src=img 
  button.removeAttribute('disabled')
}
function buttonDisable(name, img) {
  let button = document.getElementById(name)
  button.src=img 
  button.setAttribute('disabled','disabled')
}
function hide(name) { document.getElementById(name).style.visibility = "hidden" }

// UI after loading
var stopButton = document.getElementById("stop")
buttonDisable("stop", "icons/120px-Gtk-media-stop.alt.png")
var dataFileButton = document.getElementById("dataFileButton")
buttonEnable("dataFileButton", "icons/120px-Gtk-media-play-ltr.svg.png")
var demoButton = document.getElementById("demo")
buttonEnable("demo", "icons/48px-Gnome-web-browser.svg.png")
hide("demoLabel")
hide("demo")

var popup = document.getElementById("popup")
popup.style.display = 'none'
var log = document.getElementById("log")
demoButton.onmouseover = function() {
  popup.style.display = 'block';
  popup.innerHTML = `click to play ${dataFileName}`
}
demoButton.onmouseout = function() {
  document.getElementById('popup').style.display = 'none';
}
var hideSomeUIelements = true
if (hideSomeUIelements) {
//  document.getElementById("stop").style.visibility = "hidden"
//  document.getElementById("dataFileButton").style.visibility = "hidden"
  document.getElementById("cushion").style.visibility = "hidden"
  document.getElementById("slider").style.visibility = "hidden"
  document.getElementById("jitometer").style.visibility = "hidden"
  document.getElementById("jitometerLabel1").style.visibility = "hidden"
  document.getElementById("jitometerLabel2").style.visibility = "hidden"
  document.getElementById("msCushion").style.visibility = "hidden"
  document.getElementById("jitometerLabel3").style.visibility = "hidden"
  document.getElementById("jitometerLabel4").style.visibility = "hidden"
}

// set initial cushion input slider value to e.g., 20ms
// though now things seem tighter so use 10
var cushion = document.getElementById("cushion")
cushion.value = 5 // resets here when loading

// set cushion value, manually or programmatically
cushion.oninput = function() {
  let c = cushion.value
  msCushion.innerHTML = Math.round(c)
  for (let i = 0; i < workerThreads.length; i++) {
///    workerThreads[i].worker.postMessage("cushion = "+c/1000) // ms -> sec
    workerThreads[i].inlineWorker.postMessage("cushion = "+c/1000) // ms -> sec
  }
}

// real-time slider input for whatever, construct like cushion
//var slider = document.getElementById("slider")
// slider.value = 50 

//slider.oninput = function() {
//  for (let i = 0; i < workerThreads.length; i++) 
//    workerThreads[i].worker.postMessage("sliderVal = "+this.value)
// inlineWorker
//}

cushion.oninput()

makeWorkerThread = function () {
  let tmp = new WorkerThread(workerThreads.length)
  workerThreads.push(tmp)
}

dataFileButton.onclick = function() {
  startGraph()
  playFromDataFileName() // defined in engine.js, throws alert if CORS policy violation in chrome
// UI while playing
  buttonEnable("stop", "icons/120px-Gtk-media-stop.svg.png")
  buttonDisable("dataFileButton", "icons/120px-Gtk-media-play-ltr.alt.png")
}

playFromDataFileName = function() { 
  let cors = false
// console.log(url.search('file'))
  if ((testedBrowser === "chrome") && (url.search('file')!=-1)) cors = true 
  if (cors) {
    document.getElementById("statusText").innerHTML = `local chrome vs. the demo: looks like we're trying to play a local file automatically which violates chrome's policy, so either try this same demo locally via firefox, or with chrome on a server or by playing it here using the local file "tides.dat" via drag and drop` 
    return    
  }
// phew... ok
  context = new (window.AudioContext || window.webkitAudioContext)()
  for (let i = 0; i < nWorkers; i++)  makeWorkerThread() // 1 per wwckScript for now
  cushion.oninput()
//  slider.oninput() 
  for (let i = 0; i < workerThreads.length; i++) {
    if (cachedColumnOfData != null) 
      workerThreads[i].go(cachedColumnOfData)
    else   
      workerThreads[i].go()
  }
// UI while playing
  buttonEnable("stop", "icons/120px-Gtk-media-stop.svg.png")
  buttonDisable("dataFileButton", "icons/120px-Gtk-media-play-ltr.alt.png")
  hide("demoLabel")
  let holder = document.getElementById('holder')
  if (holder!=null) holder.innerHTML = "<br><br><br>"+dataFileName
}

demoButton.onclick = function() { // can demo if firefox, or if chrome and not local
  hide("demo")
  playFromDataFileName()
}

function stopPlay() {
  for (let i = 0; i < workerThreads.length; i++) workerThreads[i].stop() // = terminate
// console.log("terminate Message posted to workerThreads")
  context.close().then( function() {
    workerThreads = []
    uwta = []
// UI while stopped
    buttonDisable("stop", "icons/120px-Gtk-media-stop.alt.png")
    buttonEnable("dataFileButton", "icons/120px-Gtk-media-play-ltr.svg.png")
    }
  )
}

// clicked programmatically or manually
// sonification script can click stop programmatically with "yield pushStopButton"
stopButton.onclick = function() {
  finish() // calls stopPlay
}

// XMLHttpRequest wrapper using callbacks
function requestDataFile(obj, mainThread) {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest()
        if (mainThread == true) xhr.overrideMimeType("text/plain")
// else workerthread and Setting overrideMimeType does not work from a Worker
        xhr.open(obj.method || "GET", obj.url)
        xhr.onload = () => {
            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr.response)
            } else {
                reject(xhr.statusText)
            }
        }
        xhr.onerror = () => reject(xhr.statusText)
        xhr.send(obj.body)
    })
}

function getOneColumn(d,col) {
  let tmp = new Array
  for (let i = 0; i < d.length; i++) {
    let row = d[i]
    let arr = row.split(",")
    tmp.push(arr[col])
// console.log(arr[col])
  }
  return tmp
}

// determine range and scaling for normalization
// return an iterator that produces the next scaled value each iteration
function prepare(d,col) { // single column 
  d.pop() // remove extra <cr> in data src
  d = getOneColumn(d,col) // defaults to col 0 in calling function
  updateRateScaledToFitTotalDuration = totalDuration/d.length
// console.log("updateRateScaledToFitTotalDuration =", updateRateScaledToFitTotalDuration)

  let min = Math.min(...d)
  let max = Math.max(...d)
  let range = max - min
  let s = 1 / range
  let scaledVal = function (v) { return (v-min)*s }
  function* f() {
    for (let i = 0; i < d.length; i++) yield scaledVal(d[i])
    return
  }
  let iterator = f()
  return iterator
}


function playFromFile(df,col) { // optional col spec for multicolumn file
  if(col == null) {
    col = 0
//    console.log("inlineWorker using col " + col)
  }
  requestDataFile({url: df})
    .then(columnOfData => {
  let d = columnOfData.split("\n")
       let data = prepare(d,col) // prepare a single column data iterator
       spork(sonify,data) // iterate the sonification
//      console.log("playing ",df)
    })
    .catch(error => {
      console.log(error)
    })
}

function playFromString(columnOfData,col) { // optional col spec for multicolumn file
  if(col == null) {
    col = 0
//    console.log("inlineWorker using col " + col)
  }
  let d = columnOfData.split(' ')
    let data = prepare(d,col) // prepare a single column data iterator
    spork(sonify,data) // iterate the sonification
    console.log("playing columnOfData")
}

function spork(fstar, ...args) {
  let ti = makeTimeIterator()
  ti.sporkScript = fstar.apply( ti, args )
  ti.nextEventAt("start") // initialize the timing recursion
}

// for now the TI array is only for assigning a sequential threadNum to a TI
function makeTimeIterator() {
  let tmp = new TimeIterator(timeIterators.length)
  timeIterators.push(tmp)
  return tmp
}

function TimeIterator(n) {
  this.threadNum = n
  postMessage("uwta["+myWorker+"].push(new Array)") 
  this.nowSec = function() { return performance.now()/1000 }
  this.last = this.nowSec()
// fix for time skew which occurs when hitting replay or reloading sooner than 10 secs
  this.now=0.1 // delay start time, in case audioContext hasn't reset rendering thread time to 0
// also interacts with the minimum ramp time in scripts

  this.timeout = function (seconds) {
    return new Promise(resolve => {
      setTimeout(() => {
          resolve()
        }, seconds*1000)
      }
    )
  }

  this.nextEventAt = async function (yieldSeconds) {
    await this.timeout(yieldSeconds)
    let elapsed = (this.nowSec() - this.last)
    this.last = this.nowSec()
    let jitter = 0
    if (yieldSeconds === "start") {} 
    else {
      jitter = elapsed-yieldSeconds
      postMessage("jitometer.value = (50 +"+jitter+"*10000)")
    }
    yieldSeconds = this.sporkScript.next().value // from "yield"
    if (yieldSeconds < 0) { 
      postMessage("stopButton.click()")
      yieldSeconds = endPause // wait for UI to quit
    }
    this.now+=yieldSeconds // advance logical time
// console.log("this.now =",this.now,"yieldSeconds =",yieldSeconds)
    this.nextEventAt(yieldSeconds-jitter)
  }

  this.dspCnt = 0
  this.dsp = function () {
    let dspNext = this.dspCnt
    this.dspCnt++
    return dspNext
  }
}

function pre2(x) { return Number.parseFloat(x).toPrecision(4)}
function see(...args) { 
  newArgs = args.map(x => pre2(x))
  return newArgs.reduce((a0, a1) => { return `${a0}\t${pre2(a1)}` }) 
}

// the two-part UGEN definition
// main thread part defines the DSP patch and returns object with handles that can be manipulated
function makeSinOsc()
{
//console.log("makeSinOsc")
//console.log(pre2(context.getOutputTimestamp().contextTime*1000))
  let o = context.createOscillator()
  let g = context.createGain()
  o.type = "sine"
  o.frequency.value = 440
  g.gain.value = 0.1
  o.connect(g)
  g.connect(context.destination)
  return { osc:o, gain:g }
}

function makeFM()
{
  let mod = context.createOscillator()
  let modGain = context.createGain()
  mod.type = "sine"
  mod.connect(modGain)
//  mod.start(0)

  let car = context.createOscillator()
  let g = context.createGain()
  car.type = "sine"
  modGain.connect(car.frequency)
  car.connect(g)
  g.connect(context.destination)

  let cFreq = 2200
  let index = 33
  let mRatio = .1
  modGain.gain.value = cFreq * index
  mod.frequency.value = cFreq * mRatio
  car.frequency.value = cFreq
  g.gain.value = 0.1
  return { osc:car, gain:g, mod:mod, modGain:modGain }
}

// worker part calls the main thread part to make a new UGEN
// and have its own object that can it can manipulate, like change its freq
// usually all this is called as a time-based part of a script that worker sporks
function SinOsc(myThread) {
  let ugens = "uwta["+myWorker+"]["+myThread.threadNum+"]"
  postMessage(ugens+".push(makeSinOsc())") // there must be a corresponding function in the main thread
  return { dsp:myThread.dsp(), now:myThread.now+cushion,
    freq: function (hz) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].osc.frequency.setValueAtTime("+hz+", "+(myThread.now+cushion)+")")
      },
    freqTarget: function (hz,dur) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].osc.frequency.linearRampToValueAtTime("+hz+", "+((myThread.now+cushion)+dur)+")")
      },
    gain: function (amp) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].gain.gain.setValueAtTime("+amp+", "+(myThread.now+cushion)+")")
      },
    gainTarget: function (amp,dur) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].gain.gain.linearRampToValueAtTime("+amp+", "+((myThread.now+cushion)+dur)+")")
      },
    start: function () {
      let n = this.dsp
      postMessage(ugens+"["+n+"].osc.start("+(myThread.now+cushion)+")")
    },
    stop: function () { // can only stop "sounds" e.g., oscillators
      let n = this.dsp
      postMessage(ugens+"["+n+"].osc.stop()")
      postMessage(ugens+"["+n+"].osc.disconnect()")
      postMessage(ugens+"["+n+"].gain.disconnect()")
    }
  }
}

// incomplete, needs modulator updates and index
function FM(myThread) {
  let ugens = "uwta["+myWorker+"]["+myThread.threadNum+"]"
  postMessage(ugens+".push(makeFM())") // there must be a corresponding function in the main thread
  return { dsp:myThread.dsp(), now:myThread.now+cushion,
    freq: function (hz) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].osc.frequency.setValueAtTime("+hz+", "+(myThread.now+cushion)+")")
      },
    freqTarget: function (hz,dur) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].osc.frequency.linearRampToValueAtTime("+hz+", "+((myThread.now+cushion)+dur)+")")
      },
    gain: function (amp) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].gain.gain.setValueAtTime("+amp+", "+(myThread.now+cushion)+")")
      },
    gainTarget: function (amp,dur) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].gain.gain.linearRampToValueAtTime("+amp+", "+((myThread.now+cushion)+dur)+")")
      },
    start: function () {
      let n = this.dsp
      postMessage(ugens+"["+n+"].osc.start("+(myThread.now+cushion)+")")
      postMessage(ugens+"["+n+"].mod.start("+(myThread.now+cushion)+")")
    },
    stop: function () { // can only stop "sounds" e.g., oscillators
      let n = this.dsp
      postMessage(ugens+"["+n+"].osc.stop()")
      postMessage(ugens+"["+n+"].osc.disconnect()")
      postMessage(ugens+"["+n+"].mod.stop()")
      postMessage(ugens+"["+n+"].mod.disconnect()")
      postMessage(ugens+"["+n+"].gain.disconnect()")
      postMessage(ugens+"["+n+"].modGain.disconnect()")
    }
  }
}

// utilities from https://github.com/hoch/WAAX/
function mtof(midi) { // altered for floating pt midi vals by CC
  if (midi <= -1500) return 0
  else if (midi > 1499) return 3.282417553401589e+38
  else return 440.0 * Math.pow(2, (midi - 69) / 12.0)
}

function lintodb(lin) {
  // if below -100dB, set to -100dB to prevent taking log of zero
  return 20.0 * (lin > 0.00001 ? (Math.log(lin) / Math.LN10) : -5.0)
}

function dbtolin(db) {
  return Math.pow(10.0, db / 20.0)
}

// inline worker from https://briangrinstead.com/blog/load-web-workers-without-a-javascript-file/
function makeWorker(script) {
  var URL = window.URL || window.webkitURL;
  var Blob = window.Blob;
  var Worker = window.Worker;
  if (!URL || !Blob || !Worker || !script) {
    return null;
  }
  var blob = new Blob([script]);
  var worker = new Worker(URL.createObjectURL(blob));
  return worker;
}

function WorkerThread(n) {
//  console.log(loc) // the href of index.html or whomever calls us
  this.threadNum = n // just 1 wt for now
  uwta.push(new Array)
  let inlineWorkerText =
    `self.addEventListener('message', function(e) { 
      if (e.data.htmldir) { // has document.location.href of index.html file
        dataFileName = e.data.htmldir + e.data.datadoc
//        console.log(dataFileName)
      } else { // is some other message
        let geval = eval
        geval(e.data) 
      }
    } ,false)`
  this.inlineWorker = makeWorker(inlineWorkerText);
  this.inlineWorker.onmessage = function(e) { eval(e.data) }
  this.inlineWorker.postMessage("myWorker = "+this.threadNum) // 1
// transfer the following definitions and variable settings into inlineWorker
////////////////
  this.inlineWorker.postMessage("dataFileName = \""+dataFileName+"\"")
  this.inlineWorker.postMessage({htmlpage: htmlpage, htmldir: htmldir, datadoc: dataFileName});
//  this.inlineWorker.postMessage("console.log(dataFileName.concat(\" from inlineWorker \"))")
  this.inlineWorker.postMessage(sonify.toString())
  this.inlineWorker.postMessage(requestDataFile.toString())
  this.inlineWorker.postMessage(prepare.toString())
  this.inlineWorker.postMessage(spork.toString())
  this.inlineWorker.postMessage(makeTimeIterator.toString())
  this.inlineWorker.postMessage(TimeIterator.toString())
  this.inlineWorker.postMessage(SinOsc.toString())
  this.inlineWorker.postMessage(FM.toString())
  this.inlineWorker.postMessage(mtof.toString())
  this.inlineWorker.postMessage(lintodb.toString())
  this.inlineWorker.postMessage(dbtolin.toString())
  this.inlineWorker.postMessage(pre2.toString())
  this.inlineWorker.postMessage(see.toString())
  this.inlineWorker.postMessage("var cushion = 0") // ms to schedule ahead, set by cushion slider
  this.inlineWorker.postMessage("var timeIterators = []") // each sporked function* has a TI
  this.inlineWorker.postMessage("var pushStopButton = -1") // a function* can signal ending with     "yield pushStopButton", which clicks global stopButton 
  this.inlineWorker.postMessage("var endPause = 99999") // long pause in timing recursion while waiting for pushStop
  this.inlineWorker.postMessage(playFromString.toString())
  this.inlineWorker.postMessage(playFromFile.toString())
  this.inlineWorker.postMessage(getOneColumn.toString())
  this.inlineWorker.postMessage("col = "+col)
  this.inlineWorker.postMessage("totalDuration = "+totalDuration)
  this.inlineWorker.postMessage("var updateRateScaledToFitTotalDuration")
////////////////
  this.go = function(columnOfData) {
//    console.log(dataFileName.concat(" from main "))
    if (columnOfData) {
      this.inlineWorker.postMessage("columnOfData = \""+columnOfData.replace(/\n/g, ' ')+"\"")
//      console.log(dataFileName.concat(" read text from dragged file "))
      this.inlineWorker.postMessage("playFromString(columnOfData,col)")
    } else {
      this.inlineWorker.postMessage("playFromFile(dataFileName,col)")
    }
  }
  this.stop = function() {
    this.inlineWorker.terminate()
//    console.log("terminate Message posted to WorkerThread")
  }
}

function startGraph() { // called when dataFileButton clicked in engine.js
  document.getElementById("statusText").innerHTML = 'Running'
  row = 0
}
function finish() { // called by end of sonify or by stopButton click
  document.getElementById('statusText').innerHTML = "Ready"
  if (soundOnly != "true")  {  
    chart.data.datasets[0].pointRadius[row-1] =  0.1
    chart.update()
  }
  stopPlay()
}

var chart
function moveGraph() { // called from sonify loop
  if (soundOnly != "true")  {
    chart.data.datasets[0].pointRadius[row] =  5
    chart.data.datasets[0].pointRadius[row-1] =  0.1
    chart.update();
    row++
  }
}
initGraph = function(data) {
  var ctx = document.getElementById('canvas').getContext('2d');
  ran = function() {
    return Math.random() * 1000
  }
  var len = data.length
  var ci = new Array;
  for (let i = 0; i < len; i++) ci.push(i) 
  var ccc = new Array;
  for (let i = 0; i < len; i++) {
    ccc.push({ x: data[i][0], y:  data[i][1]} ) 
  }
  var co = new Array;
  for (let i = 0; i < len; i++) co.push("Black") 
  var cr = new Array;
  for (let i = 0; i < len; i++) cr.push(0.1) 
    var options = {
      type: 'scatter',
      data: {
        labels: ci,
        datasets: [{
          lineTension: 0,
          data: ccc,
          label: 'My First dataset',
          pointBackgroundColor: co,
          pointBorderColor: co,
          pointRadius: cr,
          showLine: true,
          fill: false,
        }]
      },
      options: {
        animation: {
          duration: 10
        },
        legend: {
          display: false
        },
        responsive: true,
        title: {
          display: true,
          text: insertName
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: insertXUnits
            },
            ticks: { min: Number(minX), max: Number(maxX) , stepSize: Number(stepX) }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: insertTitleUnits
            }
          }]
        }
      }
    }
    return new Chart(ctx, options);
  }

// determine range and scaling for normalization
function extractColumns(d) { // multi column 
  d.pop() // remove extra <cr> in data src
  d0 = getOneColumn(d,0) 
  d1 = getOneColumn(d,1) 
  let tmp = new Array
  for (let i = 0; i < d.length; i++) tmp.push( [d0[i],d1[i]] )
  return tmp
}

function loadGraphFile(df) {
  requestDataFile({url: df}, true)
    .then(columnOfData => {
      let d = columnOfData.split("\n")
      console.log("read ",df)
      let data = extractColumns(d,col)
      chart = initGraph(data)
    })
    .catch(error => {
      console.log(error)
    })
}

if (soundOnly != "true") { 
  loadGraphFile(htmldir + dataFileName) 
}

