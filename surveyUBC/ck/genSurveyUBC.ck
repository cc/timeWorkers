// chuck -s genSurveyUBC.ck

FileIO dir;
dir.open(me.dir()+"../html/data");
for (0=>int i; i<dir.dirList().cap(); i++) {
//for (0=>int i; i<1; i++) {
  <<<me.dir()+"../html/data/"+dir.dirList()[i],dir.dirList()[i].rfind(".dat")>>>;
//////////////////////////////////
  dir.dirList()[i] => string name;
  name.erase(name.rfind(".dat"),4); // .dat
  ["Sound","Animation","Sound and Animation"] @=> string type[];
  for (0=>int j; j<3; j++) {
    FileIO fin;
    fin.open(me.dir()+"../htmlTemplate/index.html", FileIO.READ);

    FileIO fout;
    fout.open(me.dir()+"../html/"+name+type[j]+".html", FileIO.WRITE);

    while (!fin.eof()) {
      fin.readLine() => string tmp;
// replace data file, titles, citations
      if (tmp.rfind("INSERT_NAME") != -1) {
        tmp.rfind("INSERT_NAME") => int index;
        tmp.erase(index,11);
        tmp.insert(index,name);
      }
      if (tmp.rfind("INSERT_TYPE") != -1) {
        tmp.rfind("INSERT_TYPE") => int index;
        tmp.erase(index,11);
        tmp.insert(index,type[j]);
      }
      if (tmp.rfind("INSERT_TITLE_NOUNITS") != -1) {
        tmp.rfind("INSERT_TITLE_NOUNITS") => int index;
        tmp.erase(index,20);
        tmp.insert(index,name2titleNoUnits(name, type[j]));
      }
      if (tmp.rfind("INSERT_TITLE_UNITS") != -1) {
        tmp.rfind("INSERT_TITLE_UNITS") => int index;
        tmp.erase(index,18);
        tmp.insert(index,name2titleUnits(name));
      }
      if (tmp.rfind("INSERT_CITATION") != -1) {
        name2citation(name) => tmp; // possible bug in .erase at 0 index, replace whole string
      }
      if (tmp.rfind("INSERT_MINX") != -1) {
        tmp.rfind("INSERT_MINX") => int index;
        tmp.erase(index,11);
        tmp.insert(index,name2minX(name));
      }
      if (tmp.rfind("INSERT_MAXX") != -1) {
        tmp.rfind("INSERT_MAXX") => int index;
        tmp.erase(index,11);
        tmp.insert(index,name2maxX(name));
      }
      if (tmp.rfind("INSERT_STEPX") != -1) {
        tmp.rfind("INSERT_STEPX") => int index;
        tmp.erase(index,12);
        tmp.insert(index,name2stepX(name));
      }
//      if (tmp.rfind("INSERT_VAXIS") != -1) {
//        tmp.rfind("INSERT_VAXIS") => int index;
//        tmp.erase(index,12);
//        tmp.insert(index,name2vAxis(name));
//      }

// set behavior for type
// don't init graph
      if (tmp.rfind("INSERT_SOUNDONLY") != -1) {
        tmp.rfind("INSERT_SOUNDONLY") => int index;
        tmp.erase(index,16);
"false" => string sound;
      if (type[j] == "Sound") "true"=> sound;
        tmp.insert(index,sound);
      }
// don't start synth
      if ((type[j] == "Animation") && (tmp.rfind("x.start()")) != -1) {
        tmp.rfind("x.start()") => int index;
        tmp.insert(index,"//");
      }
      fout <= tmp + "\n";
    }
    fout.close();
  }
}

fun string name2titleNoUnits(string name, string type) {
if (name == "iceMin" ) return "Arctic Sea Ice Minimum";
if (name == "temperature" ) return "Temperature";
if (name == "glaciers" ) return "Glaciers";
if (name == "CO2" ) return "CO\\u2082 ";
}
fun string name2titleUnits(string name) {
if (name == "iceMin" ) return "Annual Minimum (Mkm\\u00B2)";
if (name == "temperature" ) return "Temperature (C)";
if (name == "glaciers" ) return "Glaciers (Mkm\\u00B2)";
if (name == "CO2" ) return "CO\\u2082 (ppm)";
}
fun string name2citation(string name) {
if (name == "iceMin" ) return "Sea Ice Concentrations from <br> NSIDC Passive Microwave Data <br> (1979 - 2015)";
if (name == "temperature" ) return "Annual mean Northern Hemisphere <br> Temperature Anomaly <br> [Nature 03265-s6 and NASA] <br> (1500 - 2016)";
if (name == "glaciers" ) return "Ice Area Observation and Prediction <br> [Nature Geoscience, 2015, <br> 2.6C MIROC GCM] <br> (2000 - 2100)";
if (name == "CO2" ) return "CO<sub>2</sub> concentration <br> [PNAS, Ammann 2007, and Mauna Loa Observatory] <br> (1500 - 2016)";
}

fun string name2minX(string name) {
if (name == "iceMin" ) return "1978";
if (name == "temperature" ) return "1500";
if (name == "glaciers" ) return "2000";
if (name == "CO2" ) return "1500";
}
fun string name2maxX(string name) {
if (name == "iceMin" ) return "2015";
if (name == "temperature" ) return "2016";
if (name == "glaciers" ) return "2100";
if (name == "CO2" ) return "2016";
}
fun string name2stepX(string name) {
if (name == "iceMin" ) return "100";
if (name == "temperature" ) return "250";
if (name == "glaciers" ) return "50";
if (name == "CO2" ) return "250";
}

//fun string name2vAxis(string name) {
//if (name == "iceMin" ) return "viewWindow: { min: 2, max: 8 }, ticks: [2,4,6,8]";
//if (name == "temperature" ) return "viewWindow: { min: -2, max: 2 }, ticks: [-2,-1,0,1,2]";
//if (name == "glaciers" ) return "viewWindow: { min: 0, max: 32000 }, ticks: [0,10000,20000,30000]";
//if (name == "CO2" ) return "viewWindow: { min: 200, max: 410 }, ticks: [200,300,400]";
//}

