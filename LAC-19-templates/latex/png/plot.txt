set xlabel "year"
unset key
set ylabel "GDP (red), CO2 (green)"
plot [1969:2016] 'gdpScaled.dat' u (1969 + ($0/12)):1 w l lt 7, 'co2Scaled.dat' u (1969 + ($0/12)):1 w l lt 2

