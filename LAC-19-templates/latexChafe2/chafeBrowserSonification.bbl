\begin{thebibliography}{10}

\bibitem{PLA}
Bill Schottstaedt,
\newblock ``Pla: A composer's idea of a language,''
\newblock {\em Computer Music Journal}, vol. 7, no. 1, pp. 11--20, 1983.

\bibitem{wessel1987}
David Wessel, Pierre Lavoie, Lee Boynton, and Yann Orlarey,
\newblock ``Midi-lisp: A lisp-based programming environment for midi on the
  macintosh,''
\newblock in {\em Audio Engineering Society Conference: 5th International
  Conference: Music and Digital Technology}, May 1987 (accessed February 2,
  2019),
\newblock http://www.aes.org/e-lib/browse.cfm?elib=4659.

\bibitem{CM}
Heinrich Taube,
\newblock ``An introduction to common music,''
\newblock {\em Computer Music Journal}, vol. 21, no. 1, pp. 29--34, 1997.

\bibitem{wang2015}
Ge~Wang, Perry~R. Cook, and Spencer Salazar,
\newblock ``Chuck: A strongly timed computer music language,''
\newblock {\em Computer Music Journal}, vol. 39, no. 4, pp. 10--29, 2015.

\bibitem{MDN:wwa}
Moz://a~MDN web docs,
\newblock {\em Using Web Workers}, 2019 (accessed February 6, 2019),
\newblock
  \url{https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers}.

\bibitem{MDN:waa}
Moz://a~MDN web docs,
\newblock {\em Web Audio API}, 2018 (accessed December 16, 2018),
\newblock \url{https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API}.

\bibitem{kramer2010}
Bruce Walker Terri Bonebright Perry~Cook Kramer, C. and John~H. Flowers,
\newblock {\em Sonification Report: Status of the Field and Research Agenda},
  2018 (accessed December 16, 2018),
\newblock
  \url{http://digitalcommons.unl.edu/psychfacpub?utm_source=digitalcommons.unl.edu%2Fpsychfacpub%2F444&utm_medium=PDF&utm_campaign=PDFCoverPages}.

\bibitem{220a}
Chris Chafe,
\newblock {\em Music 220A}, 2019 (accessed February 6, 2019),
\newblock \url{https://ccrma.stanford.edu/courses/220a/}.

\bibitem{WAAX}
Honchan Choi,
\newblock {\em Web Audio API eXtension}, 2019 (accessed January 28, 2019),
\newblock http://hoch.github.io/WAAX/.

\bibitem{MDN:generators}
Moz://a~MDN web docs,
\newblock {\em Iterators and generators}, 2018 (accessed December 16, 2018),
\newblock
  \url{https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators}.

\bibitem{repo}
Chris Chafe,
\newblock {\em project software repository}, 2018 (accessed December 16, 2018),
\newblock \url{https://cm-gitlab.stanford.edu/cc/sonify}.

\bibitem{MDN:worklet}
Moz://a~MDN web docs,
\newblock {\em Worklet}, 2018 (accessed December 16, 2018),
\newblock \url{https://developer.mozilla.org/en-US/docs/Web/API/Worklet}.

\bibitem{MDN:async}
Moz://a~MDN web docs,
\newblock {\em async function}, 2018 (accessed December 16, 2018),
\newblock
  \url{https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function}.

\bibitem{Kondak2017}
Tianchu (Alex); Tomlinson Brianna; Walker Bruce~N. Kondak, Zachary;~Liang,
\newblock ``Web sonification sandbox - an easy-to-use web application for
  sonifying data and equations,''
\newblock {\em Proceedings of 3rd Web Audio Conference}, 2017.

\bibitem{MDN:waaw}
Hongchan Choi,
\newblock {\em Audio Worklet Design Pattern}, 2018 (accessed December 16,
  2018),
\newblock
  \url{https://developers.google.com/web/updates/2018/06/audio-worklet-design-pattern}.

\bibitem{Baumann2018}
Jan-Torsten~Milde Baumann, Christian and Johanna~Friederike Baarlink,
\newblock {\em Body Movement Sonification using the Web Audio API}, 2018
  (accessed December 16, 2018),
\newblock
  \url{https://webaudioconf.com/demos-and-posters/body-movement-sonification-using-the-web-audio-api/}.

\bibitem{Parvizi2018}
Josef Parvizi, Kapil Gururangan, Babak Razavi, and Chris Chafe,
\newblock ``Detecting silent seizures by their sound,''
\newblock {\em Epilepsia}, vol. 59, no. 4, pp. 877--884, 2018.

\end{thebibliography}
