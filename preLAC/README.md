this is a public project on
> https://cm-gitlab.stanford.edu/public

copy a read-only snapshot with the download button

or clone it to a repository on a local machine 
> git clone git@cm-gitlab.stanford.edu:cc/sonify.git

(the repo's origin requires permission to be modified)
****
# uses chuck-like syntax
****
# run from local copy
download this git project and go to the example subdirectory

open index.html with firefox

plays the (local) data file specified in index.html using the sonify function in index.html

****
# run from web (web data file)
****
firefox 
point browser to [project directory](https://ccrma.stanford.edu/~cc/sonify/example) and 
play the [data file](https://ccrma.stanford.edu/~cc/sonify/example/xxx.dat) 

****
# run from web (local data file)
****
firefox or chrome
point browser to [project directory](https://ccrma.stanford.edu/~cc/sonify/example)
drag and drop a local data file