// main.js 
// interfaces with audio context (not using audioworklet, yet because not in FF)
// spawns WorkerThreads and manages their lifecycle 

//////// testing at RPI workshop 1-Nov-2018
// typical newline problem occurs with some data files from windows users
// some version of this function for making double new lines might do the trick
// https://www.sitepoint.com/line-endings-in-javascript/
//function doublenewlines(text) { text = text.replace(/(rn|r|n)/g, 'n'); return text.replace(doublenewlinesRE, "$1nn$2"); }

// data file with 1500 lines failed, 1024 ok
///////////////////////////////////////////

// check whether chrome or firefox -- no others 
var testedBrowser = null
if(navigator.userAgent.indexOf("Chrome") != -1 )
    {
        testedBrowser = "chrome"
    }
if(navigator.userAgent.indexOf("Firefox") != -1 ) 
    {
        testedBrowser = "firefox"
    }
if (testedBrowser === null) alert("successfully tested on firefox, chrome, edge -- may fail on safari and others, continue and find out")

// specify local data file using drag and drop, required by chrome
// modified from http://html5demos.com/file-api
var holder = document.getElementById('holder')
holder.innerHTML = '<br><br><br>Drag a data file from your desktop here to play it in the browser'
holder.ondragover = function() {
    this.className = 'hover';
    return false;
};
holder.ondragend = function() {
    this.className = '';
    return false;
};
holder.ondrop = function(e) {
  this.className = '';
  e.preventDefault();
  var file = e.dataTransfer.files[0]
  reader = new FileReader();
  reader.onload = function(event) {
    draggedData(event.target.result)
  };
  reader.readAsText(file);
  holder.innerHTML = "<br><br><br>"+file.name
  return false;
};

var cachedColumnOfData = null
// play data contained in dragged in file 
function draggedData(columnOfData) {
  cachedColumnOfData = columnOfData
  context = new (window.AudioContext || window.webkitAudioContext)()
  for (let i = 0; i < nWorkers; i++)  makeWorkerThread() // 1 per wwckScript for now
  cushion.oninput()
//  slider.oninput() 
  for (let i = 0; i < workerThreads.length; i++)   
    workerThreads[i].go(columnOfData)
// UI while playing
  buttonEnable("stop", "icons/120px-Gtk-media-stop.svg.png")
  buttonDisable("dataFileButton", "icons/120px-Gtk-media-play-ltr.alt.png")
  buttonDisable("demo", "icons/48px-Gnome-web-browser.svg.png")
  hide("demoLabel")
  hide("demo")
}

///////////////////////
var nWorkers = 1 // could have parallel workers, but not in this example
var context
var workerThreads = []
var uwta = [] // ugenWorkerThreadsArrays, set of all ugens in a wt

// get and set document elements
function buttonEnable(name, img) {
  let button = document.getElementById(name)
  button.src=img 
  button.removeAttribute('disabled')
}
function buttonDisable(name, img) {
  let button = document.getElementById(name)
  button.src=img 
  button.setAttribute('disabled','disabled')
}
function hide(name) { document.getElementById(name).style.visibility = "hidden" }


// UI after loading
var stopButton = document.getElementById("stop")
buttonDisable("stop", "icons/120px-Gtk-media-stop.alt.png")
var playButton = document.getElementById("dataFileButton")
buttonDisable("dataFileButton", "icons/120px-Gtk-media-play-ltr.alt.png")
var demoButton = document.getElementById("demo")
buttonEnable("demo", "icons/48px-Gnome-web-browser.svg.png")

var popup = document.getElementById("popup")
popup.style.display = 'none'
var log = document.getElementById("log")
demoButton.onmouseover = function() {
  popup.style.display = 'block';
  popup.innerHTML = `click to play ${dataFileName}`
}
demoButton.onmouseout = function() {
  document.getElementById('popup').style.display = 'none';
}
var hideSomeUIelements = true
if (hideSomeUIelements) {
//  document.getElementById("stop").style.visibility = "hidden"
//  document.getElementById("dataFileButton").style.visibility = "hidden"
  document.getElementById("cushion").style.visibility = "hidden"
  document.getElementById("slider").style.visibility = "hidden"
  document.getElementById("jitometer").style.visibility = "hidden"
  document.getElementById("jitometerLabel1").style.visibility = "hidden"
  document.getElementById("jitometerLabel2").style.visibility = "hidden"
  document.getElementById("msCushion").style.visibility = "hidden"
  document.getElementById("jitometerLabel3").style.visibility = "hidden"
  document.getElementById("jitometerLabel4").style.visibility = "hidden"
}

// set initial cushion input slider value to e.g., 20ms
// though now things seem tighter so use 10
var cushion = document.getElementById("cushion")
cushion.value = 5 // resets here when loading

// set cushion value, manually or programmatically
cushion.oninput = function() {
  let c = cushion.value
  msCushion.innerHTML = Math.round(c)
  for (let i = 0; i < workerThreads.length; i++) {
///    workerThreads[i].worker.postMessage("cushion = "+c/1000) // ms -> sec
    workerThreads[i].inlineWorker.postMessage("cushion = "+c/1000) // ms -> sec
  }
}

// real-time slider input for whatever, construct like cushion
//var slider = document.getElementById("slider")
// slider.value = 50 

//slider.oninput = function() {
//  for (let i = 0; i < workerThreads.length; i++) 
//    workerThreads[i].worker.postMessage("sliderVal = "+this.value)
// inlineWorker
//}

cushion.oninput()

makeWorkerThread = function () {
  let tmp = new WorkerThread(workerThreads.length)
  workerThreads.push(tmp)
}

playButton.onclick = function() {
  context = new (window.AudioContext || window.webkitAudioContext)()
  for (let i = 0; i < nWorkers; i++)  makeWorkerThread() // 1 per wwckScript for now
  cushion.oninput()
//  slider.oninput() 
  for (let i = 0; i < workerThreads.length; i++) {
    if (cachedColumnOfData != null) 
      workerThreads[i].go(cachedColumnOfData)
    else   
      workerThreads[i].go()
  }
// UI while playing
  buttonEnable("stop", "icons/120px-Gtk-media-stop.svg.png")
  buttonDisable("dataFileButton", "icons/120px-Gtk-media-play-ltr.alt.png")
}

demoButton.onclick = function() { // can demo if firefox, or if chrome and not local
  hide("demo")

  let cors = false
  if ((testedBrowser === "chrome") && (loc.search('file')!=-1)) cors = true 
  if (cors) {
    document.getElementById("log").innerHTML = `local chrome vs. the demo: looks like we're trying to play a local file automatically which violates chrome's policy, so either try this same demo locally via firefox, or with chrome on a server or by playing it here using the local file "tides.dat" via drag and drop` 
    return    
  }
// phew... ok
  context = new (window.AudioContext || window.webkitAudioContext)()
  for (let i = 0; i < nWorkers; i++)  makeWorkerThread() // 1 per wwckScript for now
  cushion.oninput()
//  slider.oninput() 
  for (let i = 0; i < workerThreads.length; i++) {
    if (cachedColumnOfData != null) 
      workerThreads[i].go(cachedColumnOfData)
    else   
      workerThreads[i].go()
  }
// UI while playing
  buttonEnable("stop", "icons/120px-Gtk-media-stop.svg.png")
  buttonDisable("dataFileButton", "icons/120px-Gtk-media-play-ltr.alt.png")
  hide("demoLabel")
  holder.innerHTML = "<br><br><br>"+dataFileName
  log.innerHTML = `You can hear the rising and then falling chirp-chirp-chirp of the 
major high tides, which get highest at the new and full moons, and then 
the slightly lower trill of two roughly equal high tides per day, which 
occurs during the quarter moons. Chris Hartley (UBC)`
}

// clicked programmatically or manually
// sonification script can click stop programmatically with "yield pushStopButton"
stopButton.onclick = function() {
  for (let i = 0; i < workerThreads.length; i++) workerThreads[i].stop() // = terminate
// console.log("terminate Message posted to workerThreads")
  context.close().then( function() {
      workerThreads = []
      uwta = []
// UI while stopped
  buttonDisable("stop", "icons/120px-Gtk-media-stop.alt.png")
  buttonEnable("dataFileButton", "icons/120px-Gtk-media-play-ltr.svg.png")
    }
  )
}

// XMLHttpRequest wrapper using callbacks
function requestDataFile(obj) {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest()
        xhr.open(obj.method || "GET", obj.url)
        xhr.onload = () => {
            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr.response)
            } else {
                reject(xhr.statusText)
            }
        }
        xhr.onerror = () => reject(xhr.statusText)
        xhr.send(obj.body)
    })
}

// determine range and scaling for normalization
// return an iterator that produces the next scaled value each iteration
function prepare(d) {
//  let d = stringOfNumbers.split("\n")
  d.pop() // remove extra <cr> in data src
  d.pop() // remove last element in array caused by split
  let min = Math.min(...d)
  let max = Math.max(...d)
  let range = max - min
  let s = 1 / range
  let scaledVal = function (v) { return (v-min)*s }
  function* f() {
    for (let i = 0; i < d.length; i++) yield scaledVal(d[i])
    return
  }
  let iterator = f()
  return iterator
}


function playFromFile(df,sp) {
  if(sp == null) {
//    console.log("inlineWorker using default SonificationParameters")
//    sp = new SonificationParameters()
  }
  requestDataFile({url: df})
    .then(columnOfData => {
  let d = columnOfData.split("\n")
       let data = prepare(d) // prepare a data iterator
       spork(sonify,data,sp) // iterate the sonification
//      console.log("playing ",df)
    })
    .catch(error => {
      console.log(error)
    })
}

function playFromString(columnOfData,sp) {
  if(sp == null) {
//    console.log("inlineWorker using default SonificationParameters")
//    sp = new SonificationParameters()
  }
  let d = columnOfData.split(' ')
    let data = prepare(d) // prepare a data iterator
    spork(sonify,data,sp) // iterate the sonification
    console.log("playing columnOfData")
}

function spork(fstar, ...args) {
  let ti = makeTimeIterator()
  ti.sporkScript = fstar.apply( ti, args )
  ti.nextEventAt("start") // initialize the timing recursion
}

// for now the TI array is only for assigning a sequential threadNum to a TI
function makeTimeIterator() {
  let tmp = new TimeIterator(timeIterators.length)
  timeIterators.push(tmp)
  return tmp
}

function TimeIterator(n) {
  this.threadNum = n
  postMessage("uwta["+myWorker+"].push(new Array)") 
  this.nowSec = function() { return performance.now()/1000 }
  this.last = this.nowSec()
// fix for time skew which occurs when hitting replay or reloading sooner than 10 secs
  this.now=0.1 // delay start time, in case audioContext hasn't reset rendering thread time to 0
// also interacts with the minimum ramp time in scripts

  this.timeout = function (seconds) {
    return new Promise(resolve => {
      setTimeout(() => {
          resolve()
        }, seconds*1000)
      }
    )
  }

  this.nextEventAt = async function (yieldSeconds) {
    await this.timeout(yieldSeconds)
    let elapsed = (this.nowSec() - this.last)
    this.last = this.nowSec()
    let jitter = 0
    if (yieldSeconds === "start") {} 
    else {
      jitter = elapsed-yieldSeconds
      postMessage("jitometer.value = (50 +"+jitter+"*10000)")
    }
    yieldSeconds = this.sporkScript.next().value // from "yield"
    if (yieldSeconds < 0) { 
      postMessage("stopButton.click()")
      yieldSeconds = endPause // wait for UI to quit
    }
    this.now+=yieldSeconds // advance logical time
// console.log("this.now =",this.now,"yieldSeconds =",yieldSeconds)
    this.nextEventAt(yieldSeconds-jitter)
  }

  this.dspCnt = 0
  this.dsp = function () {
    let dspNext = this.dspCnt
    this.dspCnt++
    return dspNext
  }
}

function pre2(x) { return Number.parseFloat(x).toPrecision(4)}
function see(...args) { 
  newArgs = args.map(x => pre2(x))
  return newArgs.reduce((a0, a1) => { return `${a0}\t${pre2(a1)}` }) 
}

// the two-part UGEN definition
// main thread part defines the DSP patch and returns object with handles that can be manipulated
function makeSinOsc()
{
//console.log("makeSinOsc")
//console.log(pre2(context.getOutputTimestamp().contextTime*1000))


  let o = context.createOscillator()
  let g = context.createGain()
  o.type = "sine"
  o.frequency.value = 440
  g.gain.value = 0.1
  o.connect(g)
  g.connect(context.destination)
  return { osc:o, gain:g }
}

// worker part calls the main thread part to make a new UGEN
// and have its own object that can it can manipulate, like change its freq
// usually all this is called as a time-based part of a script that worker sporks
function SinOsc(myThread) {
  let ugens = "uwta["+myWorker+"]["+myThread.threadNum+"]"
  postMessage(ugens+".push(makeSinOsc())") // there must be a corresponding function in the main thread
  return { dsp:myThread.dsp(), now:myThread.now+cushion,
    freq: function (hz) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].osc.frequency.setValueAtTime("+hz+", "+(myThread.now+cushion)+")")
      },
    freqTarget: function (hz,dur) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].osc.frequency.linearRampToValueAtTime("+hz+", "+((myThread.now+cushion)+dur)+")")
      },
    gain: function (amp) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].gain.gain.setValueAtTime("+amp+", "+(myThread.now+cushion)+")")
      },
    gainTarget: function (amp,dur) {
        let n = this.dsp
        postMessage(ugens+"["+n+"].gain.gain.linearRampToValueAtTime("+amp+", "+((myThread.now+cushion)+dur)+")")
      },
    start: function () {
      let n = this.dsp
      postMessage(ugens+"["+n+"].osc.start("+(myThread.now+cushion)+")")
    },
    stop: function () { // can only stop "sounds" e.g., oscillators
      let n = this.dsp
      postMessage(ugens+"["+n+"].osc.stop()")
      postMessage(ugens+"["+n+"].osc.disconnect()")
      postMessage(ugens+"["+n+"].gain.disconnect()")
    }
  }
}

// utilities from https://github.com/hoch/WAAX/
function mtof(midi) { // altered for floating pt midi vals by CC
  if (midi <= -1500) return 0
  else if (midi > 1499) return 3.282417553401589e+38
  else return 440.0 * Math.pow(2, (midi - 69) / 12.0)
}

function lintodb(lin) {
  // if below -100dB, set to -100dB to prevent taking log of zero
  return 20.0 * (lin > 0.00001 ? (Math.log(lin) / Math.LN10) : -5.0)
}

function dbtolin(db) {
  return Math.pow(10.0, db / 20.0)
}

// inline worker from https://briangrinstead.com/blog/load-web-workers-without-a-javascript-file/
function makeWorker(script) {
  var URL = window.URL || window.webkitURL;
  var Blob = window.Blob;
  var Worker = window.Worker;
  if (!URL || !Blob || !Worker || !script) {
    return null;
  }
  var blob = new Blob([script]);
  var worker = new Worker(URL.createObjectURL(blob));
  return worker;
}

function WorkerThread(n) {
//  console.log(loc) // the href of index.html or whomever calls us
  this.threadNum = n // just 1 wt for now
  uwta.push(new Array)
  let inlineWorkerText =
    `self.addEventListener('message', function(e) { 
      if (e.data.docloc) { // has document.location.href of index.html file
        var url = e.data.docloc;
        var index = url.indexOf('index.html');
        if (index != -1) url = url.substring(0, index);
        dataFileName = url + e.data.datadoc
//        console.log(dataFileName)
      } else { // is some other message
        let geval = eval
        geval(e.data) 
      }
    } ,false)`
  this.inlineWorker = makeWorker(inlineWorkerText);
  this.inlineWorker.onmessage = function(e) { eval(e.data) }
  this.inlineWorker.postMessage("myWorker = "+this.threadNum) // 1
// transfer the following definitions and variable settings into inlineWorker
////////////////
  this.inlineWorker.postMessage("dataFileName = \""+dataFileName+"\"")
  this.inlineWorker.postMessage({docloc: loc, datadoc: dataFileName});
//  this.inlineWorker.postMessage("console.log(dataFileName.concat(\" from inlineWorker \"))")
  this.inlineWorker.postMessage(sonify.toString())
  this.inlineWorker.postMessage(requestDataFile.toString())
  this.inlineWorker.postMessage(prepare.toString())
  this.inlineWorker.postMessage(spork.toString())
  this.inlineWorker.postMessage(makeTimeIterator.toString())
  this.inlineWorker.postMessage(TimeIterator.toString())
  this.inlineWorker.postMessage(SinOsc.toString())
  this.inlineWorker.postMessage(mtof.toString())
  this.inlineWorker.postMessage(lintodb.toString())
  this.inlineWorker.postMessage(dbtolin.toString())
  this.inlineWorker.postMessage(pre2.toString())
  this.inlineWorker.postMessage(see.toString())
  this.inlineWorker.postMessage("var cushion = 0") // ms to schedule ahead, set by cushion slider
  this.inlineWorker.postMessage("var timeIterators = []") // each sporked function* has a TI
  this.inlineWorker.postMessage("var pushStopButton = -1") // a function* can signal ending with     "yield pushStopButton", which clicks global stopButton 
  this.inlineWorker.postMessage("var endPause = 99999") // long pause in timing recursion while waiting for pushStop
  this.inlineWorker.postMessage(playFromString.toString())
  this.inlineWorker.postMessage(playFromFile.toString())
////////////////
  this.go = function(columnOfData) {
//    console.log(dataFileName.concat(" from main "))
    if (columnOfData) {
      this.inlineWorker.postMessage("columnOfData = \""+columnOfData.replace(/\n/g, ' ')+"\"")
//      console.log(dataFileName.concat(" read text from dragged file "))
      this.inlineWorker.postMessage("playFromString(columnOfData)")
    } else {
      this.inlineWorker.postMessage("playFromFile(dataFileName)")
    }
  }
  this.stop = function() {
    this.inlineWorker.terminate()
//    console.log("terminate Message posted to WorkerThread")
  }
}

